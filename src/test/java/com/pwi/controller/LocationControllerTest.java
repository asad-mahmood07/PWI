package com.pwi.controller;

import com.pwi.model.Employee;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import static org.mockito.Mockito.atLeastOnce;

import org.springframework.context.MessageSource;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


import com.pwi.model.Location;
import com.pwi.service.LocationService;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

public class LocationControllerTest {

	@Mock
	LocationService service;
	
	@Mock
	MessageSource message;
	
	@InjectMocks
	LocationController appController;
	
	@Spy
	List<Location> locations = new ArrayList<Location>();

	@Spy
	ModelMap model;
	
	@Mock
	BindingResult result;
	
	@BeforeClass
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		//locations = getEmployeeList();
	}
	
	@Test
	public void listLocations(){
		when(service.findAllLocations()).thenReturn(locations);
		Assert.assertEquals(appController.listLocations(model), "allLocations");
		Assert.assertEquals(model.get("locations"), locations);
		verify(service, atLeastOnce()).findAllLocations();
	}
	
        
//	@Test
//	public void saveLocationWithValidationError(){
//		when(result.hasErrors()).thenReturn(true);
//		doNothing().when(service).createLocation(any(Location.class));
//		Assert.assertEquals(appController.saveLocation(locations.get(0), result, model), "registration");
//	}
}
