package com.pwi.service;

import java.util.List;

import com.pwi.model.Location;

public interface LocationService {

	Location findById(int id);
	
	void createLocation(Location location);
	
	void updateLocation(Location location);
	
	List<Location> findAllLocations(); 
	
	
	
}
