package com.pwi.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pwi.dao.LocationDao;
import com.pwi.model.Location;
import com.pwi.service.LocationService;

@Service("locationService")
@Transactional
public class LocationServiceImpl implements LocationService {

	@Autowired
	private LocationDao dao;
	
	public Location findById(int id) {
		return dao.findById(id);
	}

	public void createLocation(Location location) {
		dao.saveLocation(location);
	}

	/*
	 * Since the method is running with Transaction, No need to call hibernate update explicitly.
	 * Just fetch the entity from db and update it with proper values within transaction.
	 * It will be updated in db once transaction ends. 
	 */
	public void updateLocation(Location location) 
        {
		Location entity = dao.findById(location.getLocationid());
		if(entity!=null)
                {
			entity.setAddress(location.getAddress());
			entity.setCode(location.getCode());
			entity.setName(location.getName());
		}
	}

	
	public List<Location> findAllLocations() {
		return dao.findAllLocations();
	}

	

	
}
