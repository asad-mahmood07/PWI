package com.pwi.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pwi.model.Location;
import com.pwi.service.LocationService;

@Controller
@RequestMapping("/")
public class LocationController {

	@Autowired
	       LocationService service;
	
	@Autowired
	MessageSource messageSource;

	
	@RequestMapping(value = { "/", "/fetchLocation" }, method = RequestMethod.GET)
	public String listLocations(ModelMap model) {

		List<Location> locations = service.findAllLocations();
		model.addAttribute("locations", locations);
		return "allLocations";
	}

	
	@RequestMapping(value = { "/create" }, method = RequestMethod.POST)
	public String saveLocation(@Valid Location location, BindingResult result,
			ModelMap model) {

		if (result.hasErrors()) {
			return "fail";
		}

	
		
		service.createLocation(location);

		model.addAttribute("success", "Location " + location.getName() + " registered successfully");
		return "success";
	}


	
}
