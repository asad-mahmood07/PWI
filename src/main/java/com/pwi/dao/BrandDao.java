package com.pwi.dao;

import java.util.List;

import com.pwi.model.Brand;

public interface BrandDao {

	Brand findById(int id);

	void saveBrand(Brand brand);
	
	List<Brand> findAllBrands();

	

}
