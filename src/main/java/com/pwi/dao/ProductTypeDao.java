package com.pwi.dao;

import java.util.List;

import com.pwi.model.ProductType;

public interface ProductTypeDao 
{

        ProductType findById(int id);

	void saveProductType(ProductType productType);
	
	List<ProductType> findAllProductTypes();

	

}
