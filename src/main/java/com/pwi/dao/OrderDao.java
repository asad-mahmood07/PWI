package com.pwi.dao;

import java.util.List;

import com.pwi.model.Order;

public interface OrderDao 
{

        Order findById(int id);

	void saveOrder(Order order);
	
	List<Order> findAllOrders();

	

}
