package com.pwi.dao.impl;

import com.pwi.dao.AbstractDao;
import com.pwi.dao.OrderDetailDao;
import com.pwi.model.OrderDetail;
import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;


@Repository("orderDetailDao")
public class OrderDetailDaoImpl extends AbstractDao<Integer, OrderDetail> implements OrderDetailDao {

        @Override
	public OrderDetail findById(int id) {
		return getByKey(id);
	}

        @Override
	public void saveOrderDetail(OrderDetail orderdetail) {
		persist(orderdetail);
	}

	@SuppressWarnings("unchecked")
        @Override
	public List<OrderDetail> findAllOrderdetails() {
		Criteria criteria = createEntityCriteria();
		return (List<OrderDetail>) criteria.list();
	}

	
}
