package com.pwi.dao.impl;

import com.pwi.dao.AbstractDao;
import com.pwi.dao.ProductTypeDao;
import com.pwi.model.ProductType;
import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;


@Repository("productTypeDao")
public class ProductTypeDaoImpl extends AbstractDao<Integer, ProductType> implements ProductTypeDao {

        @Override
	public ProductType findById(int id) {
		return getByKey(id);
	}

        @Override
	public void saveProductType(ProductType productType) {
		persist(productType);
	}

	@SuppressWarnings("unchecked")
        @Override
	public List<ProductType> findAllProductTypes() {
		Criteria criteria = createEntityCriteria();
		return (List<ProductType>) criteria.list();
	}

	
}
