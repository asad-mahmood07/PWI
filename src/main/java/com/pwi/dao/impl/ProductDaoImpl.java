package com.pwi.dao.impl;

import com.pwi.dao.AbstractDao;
import com.pwi.dao.ProductDao;
import com.pwi.model.Product;
import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;


@Repository("productDao")
public class ProductDaoImpl extends AbstractDao<Integer, Product> implements ProductDao {

        @Override
	public Product findById(int id) {
		return getByKey(id);
	}

        @Override
	public void saveProduct(Product product) {
		persist(product);
	}

	@SuppressWarnings("unchecked")
        @Override
	public List<Product> findAllProducts() {
		Criteria criteria = createEntityCriteria();
		return (List<Product>) criteria.list();
	}

	
}
