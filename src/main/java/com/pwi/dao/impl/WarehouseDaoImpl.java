package com.pwi.dao.impl;

import com.pwi.dao.AbstractDao;
import com.pwi.dao.WarehouseDao;
import com.pwi.model.Warehouse;
import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;


@Repository("warehouseDao")
public class WarehouseDaoImpl extends AbstractDao<Integer, Warehouse> implements WarehouseDao {

        @Override
	public Warehouse findById(int id) {
		return getByKey(id);
	}

        @Override
	public void saveWarehouse(Warehouse warehouse) {
		persist(warehouse);
	}

	@SuppressWarnings("unchecked")
        @Override
	public List<Warehouse> findAllWarehouses() {
		Criteria criteria = createEntityCriteria();
		return (List<Warehouse>) criteria.list();
	}

	
}
