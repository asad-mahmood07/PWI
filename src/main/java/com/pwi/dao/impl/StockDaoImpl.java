package com.pwi.dao.impl;

import com.pwi.dao.AbstractDao;
import com.pwi.dao.StockDao;
import com.pwi.model.Stock;
import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;


@Repository("stockDao")
public class StockDaoImpl extends AbstractDao<Integer, Stock> implements StockDao {

        @Override
	public Stock findById(int id) {
		return getByKey(id);
	}

        @Override
	public void saveStock(Stock stock) {
		persist(stock);
	}

	@SuppressWarnings("unchecked")
        @Override
	public List<Stock> findAllStocks() {
		Criteria criteria = createEntityCriteria();
		return (List<Stock>) criteria.list();
	}

	
}
