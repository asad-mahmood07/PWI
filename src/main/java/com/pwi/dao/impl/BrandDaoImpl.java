package com.pwi.dao.impl;

import com.pwi.dao.AbstractDao;
import com.pwi.dao.BrandDao;
import com.pwi.model.Brand;
import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;


@Repository("brandDao")
public class BrandDaoImpl extends AbstractDao<Integer, Brand> implements BrandDao {

        @Override
	public Brand findById(int id) {
		return getByKey(id);
	}

        @Override
	public void saveBrand(Brand brand) {
		persist(brand);
	}

	@SuppressWarnings("unchecked")
        @Override
	public List<Brand> findAllBrands() {
		Criteria criteria = createEntityCriteria();
		return (List<Brand>) criteria.list();
	}

	
}
