package com.pwi.dao.impl;

import com.pwi.dao.AbstractDao;
import com.pwi.dao.LocationDao;
import com.pwi.model.Location;
import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;


@Repository("locationDao")
public class LocationDaoImpl extends AbstractDao<Integer, Location> implements LocationDao {

        @Override
	public Location findById(int id) {
		return getByKey(id);
	}

        @Override
	public void saveLocation(Location location) {
		persist(location);
	}

	@SuppressWarnings("unchecked")
        @Override
	public List<Location> findAllLocations() {
		Criteria criteria = createEntityCriteria();
		return (List<Location>) criteria.list();
	}

	
}
