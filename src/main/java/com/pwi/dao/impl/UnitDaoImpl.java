package com.pwi.dao.impl;

import com.pwi.dao.AbstractDao;
import com.pwi.dao.UnitDao;
import com.pwi.model.Unit;
import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;


@Repository("unitDao")
public class UnitDaoImpl extends AbstractDao<Integer, Unit> implements UnitDao {

        @Override
	public Unit findById(int id) {
		return getByKey(id);
	}

        @Override
	public void saveUnit(Unit unit) {
		persist(unit);
	}

	@SuppressWarnings("unchecked")
        @Override
	public List<Unit> findAllUnits() {
		Criteria criteria = createEntityCriteria();
		return (List<Unit>) criteria.list();
	}

	
}
