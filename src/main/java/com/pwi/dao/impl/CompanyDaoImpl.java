package com.pwi.dao.impl;

import com.pwi.dao.AbstractDao;
import com.pwi.dao.CompanyDao;
import com.pwi.model.Company;
import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;


@Repository("companyDao")
public class CompanyDaoImpl extends AbstractDao<Integer, Company> implements CompanyDao {

        @Override
	public Company findById(int id) {
		return getByKey(id);
	}

        @Override
	public void saveCompany(Company company) {
		persist(company);
	}

	@SuppressWarnings("unchecked")
        @Override
	public List<Company> findAllCompanies() {
		Criteria criteria = createEntityCriteria();
		return (List<Company>) criteria.list();
	}

	
}
