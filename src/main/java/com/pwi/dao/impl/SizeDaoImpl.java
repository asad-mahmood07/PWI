package com.pwi.dao.impl;

import com.pwi.dao.AbstractDao;
import com.pwi.dao.SizeDao;
import com.pwi.model.Size;
import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;


@Repository("sizeDao")
public class SizeDaoImpl extends AbstractDao<Integer, Size> implements SizeDao {

        @Override
	public Size findById(int id) {
		return getByKey(id);
	}

        @Override
	public void saveSize(Size size) {
		persist(size);
	}

	@SuppressWarnings("unchecked")
        @Override
	public List<Size> findAllSizes() {
		Criteria criteria = createEntityCriteria();
		return (List<Size>) criteria.list();
	}

	
}
