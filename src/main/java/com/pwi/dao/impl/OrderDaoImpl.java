package com.pwi.dao.impl;

import com.pwi.dao.AbstractDao;
import com.pwi.dao.OrderDao;
import com.pwi.model.Order;
import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;


@Repository("orderDao")
public class OrderDaoImpl extends AbstractDao<Integer, Order> implements OrderDao {

        @Override
	public Order findById(int id) {
		return getByKey(id);
	}

        @Override
	public void saveOrder(Order order) {
		persist(order);
	}

	@SuppressWarnings("unchecked")
        @Override
	public List<Order> findAllOrders() {
		Criteria criteria = createEntityCriteria();
		return (List<Order>) criteria.list();
	}

	
}
