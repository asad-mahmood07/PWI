package com.pwi.dao;

import java.util.List;

import com.pwi.model.Warehouse;

public interface WarehouseDao 
{

        Warehouse findById(int id);

	void saveWarehouse(Warehouse warehouse);
	
	List<Warehouse> findAllWarehouses();

	

}
