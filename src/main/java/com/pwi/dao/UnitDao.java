package com.pwi.dao;

import java.util.List;

import com.pwi.model.Stock;
import com.pwi.model.Unit;

public interface UnitDao 
{

        Unit findById(int id);

	void saveUnit(Unit unit);
	
	List<Unit> findAllUnits();

	

}
