package com.pwi.dao;

import java.util.List;

import com.pwi.model.Size;

public interface SizeDao 
{

        Size findById(int id);

	void saveSize(Size size);
	
	List<Size> findAllSizes();

	

}
