package com.pwi.dao;

import java.util.List;

import com.pwi.model.Location;

public interface LocationDao {

	   Location findById(int id);

	void saveLocation(Location location);
	
	List<Location> findAllLocations();

	

}
