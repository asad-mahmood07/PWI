package com.pwi.dao;

import java.util.List;

import com.pwi.model.Stock;

public interface StockDao 
{

        Stock findById(int id);

	void saveStock(Stock stock);
	
	List<Stock> findAllStocks();

	

}
