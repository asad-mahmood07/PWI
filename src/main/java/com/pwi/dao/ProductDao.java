package com.pwi.dao;

import java.util.List;

import com.pwi.model.Order;
import com.pwi.model.Product;

public interface ProductDao 
{

        Product findById(int id);

	void saveProduct(Product product);
	
	List<Product> findAllProducts();

	

}
