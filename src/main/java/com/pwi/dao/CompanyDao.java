package com.pwi.dao;

import java.util.List;

import com.pwi.model.Company;

public interface CompanyDao {

	   Company findById(int id);

	void saveCompany(Company company);
	
	List<Company> findAllCompanies();

	

}
