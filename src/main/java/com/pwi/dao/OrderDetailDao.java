package com.pwi.dao;

import java.util.List;

import com.pwi.model.OrderDetail;

public interface OrderDetailDao 
{

        OrderDetail findById(int id);

	void saveOrderDetail(OrderDetail orderdetail);
	
	List<OrderDetail> findAllOrderdetails();

	

}
