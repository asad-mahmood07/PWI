package com.pwi.model;


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="location")
public class Location  implements java.io.Serializable {


     private int locationid;
     private Company company;
     private String name;
     private String code;
     private String address;
     private Set<Warehouse> warehouses = new HashSet<Warehouse>(0);
     private Set<Order> orders = new HashSet<Order>(0);

    public Location() {
    }

	
    public Location(int locationid) {
        this.locationid = locationid;
    }
    public Location(int locationid, Company company, String name, String code, String address, Set<Warehouse> warehouses, Set<Order> orders) {
       this.locationid = locationid;
       this.company = company;
       this.name = name;
       this.code = code;
       this.address = address;
       this.warehouses = warehouses;
       this.orders = orders;
    }
   
     @Id 

    
    @Column(name="locationid", unique=true, nullable=false)
    public int getLocationid() {
        return this.locationid;
    }
    
    public void setLocationid(int locationid) {
        this.locationid = locationid;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="companyid")
    public Company getCompany() {
        return this.company;
    }
    
    public void setCompany(Company company) {
        this.company = company;
    }

    
    @Column(name="name")
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    
    @Column(name="code")
    public String getCode() {
        return this.code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }

    
    @Column(name="address")
    public String getAddress() {
        return this.address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="location")
    public Set<Warehouse> getWarehouses() {
        return this.warehouses;
    }
    
    public void setWarehouses(Set<Warehouse> warehouses) {
        this.warehouses = warehouses;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="location")
    public Set<Order> getOrders() {
        return this.orders;
    }
    
    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }




}


