package com.pwi.model;


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="producttype")
public class ProductType  implements java.io.Serializable {


     private int producttypeid;
     private String name;
     private String description;
     private Set<Product> products = new HashSet<Product>(0);

    public ProductType() {
    }

	
    public ProductType(int producttypeid) {
        this.producttypeid = producttypeid;
    }
    public ProductType(int producttypeid, String name, String description, Set<Product> products) {
       this.producttypeid = producttypeid;
       this.name = name;
       this.description = description;
       this.products = products;
    }
   
     @Id 

    
    @Column(name="producttypeid", unique=true, nullable=false)
    public int getProducttypeid() {
        return this.producttypeid;
    }
    
    public void setProducttypeid(int producttypeid) {
        this.producttypeid = producttypeid;
    }

    
    @Column(name="name")
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    
    @Column(name="description")
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="producttype")
    public Set<Product> getProducts() {
        return this.products;
    }
    
    public void setProducts(Set<Product> products) {
        this.products = products;
    }




}


