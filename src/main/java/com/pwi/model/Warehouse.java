package com.pwi.model;


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="warehouse")
public class Warehouse  implements java.io.Serializable {


     private int warehouseid;
     private Location location;
     private String name;
     private String description;
     private Boolean blnIsactive;
     private Set<Stock> stocks = new HashSet<Stock>(0);

    public Warehouse() {
    }

	
    public Warehouse(int warehouseid) {
        this.warehouseid = warehouseid;
    }
    public Warehouse(int warehouseid, Location location, String name, String description, Boolean blnIsactive, Set<Stock> stocks) {
       this.warehouseid = warehouseid;
       this.location = location;
       this.name = name;
       this.description = description;
       this.blnIsactive = blnIsactive;
       this.stocks = stocks;
    }
   
     @Id 

    
    @Column(name="warehouseid", unique=true, nullable=false)
    public int getWarehouseid() {
        return this.warehouseid;
    }
    
    public void setWarehouseid(int warehouseid) {
        this.warehouseid = warehouseid;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="locationid")
    public Location getLocation() {
        return this.location;
    }
    
    public void setLocation(Location location) {
        this.location = location;
    }

    
    @Column(name="name")
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    
    @Column(name="description")
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    
    @Column(name="bln_isactive")
    public Boolean getBlnIsactive() {
        return this.blnIsactive;
    }
    
    public void setBlnIsactive(Boolean blnIsactive) {
        this.blnIsactive = blnIsactive;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="warehouse")
    public Set<Stock> getStocks() {
        return this.stocks;
    }
    
    public void setStocks(Set<Stock> stocks) {
        this.stocks = stocks;
    }




}


