package com.pwi.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="orderdetail")
public class OrderDetail  implements java.io.Serializable {


     private int orderdetailid;
     private Order order;
     private Product product;
     private Long orderqty;
     private Long unitprice;
     private Long total;

    public OrderDetail() {
    }

	
    public OrderDetail(int orderdetailid) {
        this.orderdetailid = orderdetailid;
    }
    public OrderDetail(int orderdetailid, Order order, Product product, Long orderqty, Long unitprice, Long total) {
       this.orderdetailid = orderdetailid;
       this.order = order;
       this.product = product;
       this.orderqty = orderqty;
       this.unitprice = unitprice;
       this.total = total;
    }
   
     @Id 

    
    @Column(name="orderdetailid", unique=true, nullable=false)
    public int getOrderdetailid() {
        return this.orderdetailid;
    }
    
    public void setOrderdetailid(int orderdetailid) {
        this.orderdetailid = orderdetailid;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="orderid")
    public Order getOrder() {
        return this.order;
    }
    
    public void setOrder(Order order) {
        this.order = order;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="productid")
    public Product getProduct() {
        return this.product;
    }
    
    public void setProduct(Product product) {
        this.product = product;
    }

    
    @Column(name="orderqty", precision=10, scale=0)
    public Long getOrderqty() {
        return this.orderqty;
    }
    
    public void setOrderqty(Long orderqty) {
        this.orderqty = orderqty;
    }

    
    @Column(name="unitprice", precision=10, scale=0)
    public Long getUnitprice() {
        return this.unitprice;
    }
    
    public void setUnitprice(Long unitprice) {
        this.unitprice = unitprice;
    }

    
    @Column(name="total", precision=10, scale=0)
    public Long getTotal() {
        return this.total;
    }
    
    public void setTotal(Long total) {
        this.total = total;
    }




}


