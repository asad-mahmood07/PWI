package com.pwi.model;


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="brand")
public class Brand  implements java.io.Serializable {


     private int brandid;
     private String name;
     private String code;
     private Set<Product> products = new HashSet<Product>(0);

    public Brand() {
    }

	
    public Brand(int brandid) {
        this.brandid = brandid;
    }
    public Brand(int brandid, String name, String code, Set<Product> products) {
       this.brandid = brandid;
       this.name = name;
       this.code = code;
       this.products = products;
    }
   
     @Id 

    
    @Column(name="brandid", unique=true, nullable=false)
    public int getBrandid() {
        return this.brandid;
    }
    
    public void setBrandid(int brandid) {
        this.brandid = brandid;
    }

    
    @Column(name="name")
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    
    @Column(name="code")
    public String getCode() {
        return this.code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="brand")
    public Set<Product> getProducts() {
        return this.products;
    }
    
    public void setProducts(Set<Product> products) {
        this.products = products;
    }




}


