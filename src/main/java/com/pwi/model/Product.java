package com.pwi.model;


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="product")
public class Product  implements java.io.Serializable {


     private int productid;
     private Brand brand;
     private ProductType producttype;
     private Size size;
     private Unit unit;
     private String name;
     private String code;
     private Integer reorderlevel;
     private Integer minlevel;
     private Integer maxlevel;
     private Integer iteminbox;
     private Set<Stock> stocks = new HashSet<Stock>(0);
     private Set<OrderDetail> orderdetails = new HashSet<OrderDetail>(0);

    public Product() {
    }

	
    public Product(int productid) {
        this.productid = productid;
    }
    public Product(int productid, Brand brand, ProductType producttype, Size size, Unit unit, String name, String code, Integer reorderlevel, Integer minlevel, Integer maxlevel, Integer iteminbox, Set<Stock> stocks, Set<OrderDetail> orderdetails) {
       this.productid = productid;
       this.brand = brand;
       this.producttype = producttype;
       this.size = size;
       this.unit = unit;
       this.name = name;
       this.code = code;
       this.reorderlevel = reorderlevel;
       this.minlevel = minlevel;
       this.maxlevel = maxlevel;
       this.iteminbox = iteminbox;
       this.stocks = stocks;
       this.orderdetails = orderdetails;
    }
   
     @Id 

    
    @Column(name="productid", unique=true, nullable=false)
    public int getProductid() {
        return this.productid;
    }
    
    public void setProductid(int productid) {
        this.productid = productid;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="brandid")
    public Brand getBrand() {
        return this.brand;
    }
    
    public void setBrand(Brand brand) {
        this.brand = brand;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="producttypeid")
    public ProductType getProducttype() {
        return this.producttype;
    }
    
    public void setProducttype(ProductType producttype) {
        this.producttype = producttype;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="sizeid")
    public Size getSize() {
        return this.size;
    }
    
    public void setSize(Size size) {
        this.size = size;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="unitid")
    public Unit getUnit() {
        return this.unit;
    }
    
    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    
    @Column(name="name")
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    
    @Column(name="code")
    public String getCode() {
        return this.code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }

    
    @Column(name="reorderlevel")
    public Integer getReorderlevel() {
        return this.reorderlevel;
    }
    
    public void setReorderlevel(Integer reorderlevel) {
        this.reorderlevel = reorderlevel;
    }

    
    @Column(name="minlevel")
    public Integer getMinlevel() {
        return this.minlevel;
    }
    
    public void setMinlevel(Integer minlevel) {
        this.minlevel = minlevel;
    }

    
    @Column(name="maxlevel")
    public Integer getMaxlevel() {
        return this.maxlevel;
    }
    
    public void setMaxlevel(Integer maxlevel) {
        this.maxlevel = maxlevel;
    }

    
    @Column(name="iteminbox")
    public Integer getIteminbox() {
        return this.iteminbox;
    }
    
    public void setIteminbox(Integer iteminbox) {
        this.iteminbox = iteminbox;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="product")
    public Set<Stock> getStocks() {
        return this.stocks;
    }
    
    public void setStocks(Set<Stock> stocks) {
        this.stocks = stocks;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="product")
    public Set<OrderDetail> getOrderdetails() {
        return this.orderdetails;
    }
    
    public void setOrderdetails(Set<OrderDetail> orderdetails) {
        this.orderdetails = orderdetails;
    }




}


