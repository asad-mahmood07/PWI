package com.pwi.model;


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="company")
public class Company  implements java.io.Serializable {


     private Integer companyid;
     private String companyname;
     private String companycode;
     private String url;
     private String address;
     private Set<Location> locations = new HashSet<Location>(0);

    public Company() {
    }

    public Company(String companyname, String companycode, String url, String address, Set<Location> locations) {
       this.companyname = companyname;
       this.companycode = companycode;
       this.url = url;
       this.address = address;
       this.locations = locations;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="companyid", unique=true, nullable=false)
    public Integer getCompanyid() {
        return this.companyid;
    }
    
    public void setCompanyid(Integer companyid) {
        this.companyid = companyid;
    }

    
    @Column(name="companyname")
    public String getCompanyname() {
        return this.companyname;
    }
    
    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    
    @Column(name="companycode")
    public String getCompanycode() {
        return this.companycode;
    }
    
    public void setCompanycode(String companycode) {
        this.companycode = companycode;
    }

    
    @Column(name="url")
    public String getUrl() {
        return this.url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }

    
    @Column(name="address")
    public String getAddress() {
        return this.address;
    }
    
    public void setAddress(String address) {
        this.address = address;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="company")
    public Set<Location> getLocations() {
        return this.locations;
    }
    
    public void setLocations(Set<Location> locations) {
        this.locations = locations;
    }




}


