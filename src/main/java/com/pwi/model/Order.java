package com.pwi.model;


import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name="order")
public class Order  implements java.io.Serializable {


     private int orderid;
     private Location location;
     private String orderno;
     private Integer supplierid;
     private Date orderdate;
     private Date deliverydate;
     private Date createddate;
     private Date modifieddate;
     private String orderstatus;
     private Set<OrderDetail> orderdetails = new HashSet<OrderDetail>(0);

    public Order() {
    }

	
    public Order(int orderid) {
        this.orderid = orderid;
    }
    public Order(int orderid, Location location, String orderno, Integer supplierid, Date orderdate, Date deliverydate, Date createddate, Date modifieddate, String orderstatus, Set<OrderDetail> orderdetails) {
       this.orderid = orderid;
       this.location = location;
       this.orderno = orderno;
       this.supplierid = supplierid;
       this.orderdate = orderdate;
       this.deliverydate = deliverydate;
       this.createddate = createddate;
       this.modifieddate = modifieddate;
       this.orderstatus = orderstatus;
       this.orderdetails = orderdetails;
    }
   
     @Id 

    
    @Column(name="orderid", unique=true, nullable=false)
    public int getOrderid() {
        return this.orderid;
    }
    
    public void setOrderid(int orderid) {
        this.orderid = orderid;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="locationid")
    public Location getLocation() {
        return this.location;
    }
    
    public void setLocation(Location location) {
        this.location = location;
    }

    
    @Column(name="orderno")
    public String getOrderno() {
        return this.orderno;
    }
    
    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    
    @Column(name="supplierid")
    public Integer getSupplierid() {
        return this.supplierid;
    }
    
    public void setSupplierid(Integer supplierid) {
        this.supplierid = supplierid;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="orderdate", length=10)
    public Date getOrderdate() {
        return this.orderdate;
    }
    
    public void setOrderdate(Date orderdate) {
        this.orderdate = orderdate;
    }

    @Temporal(TemporalType.DATE)
    @Column(name="deliverydate", length=10)
    public Date getDeliverydate() {
        return this.deliverydate;
    }
    
    public void setDeliverydate(Date deliverydate) {
        this.deliverydate = deliverydate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="createddate", length=19)
    public Date getCreateddate() {
        return this.createddate;
    }
    
    public void setCreateddate(Date createddate) {
        this.createddate = createddate;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="modifieddate", length=19)
    public Date getModifieddate() {
        return this.modifieddate;
    }
    
    public void setModifieddate(Date modifieddate) {
        this.modifieddate = modifieddate;
    }

    
    @Column(name="orderstatus")
    public String getOrderstatus() {
        return this.orderstatus;
    }
    
    public void setOrderstatus(String orderstatus) {
        this.orderstatus = orderstatus;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="order")
    public Set<OrderDetail> getOrderdetails() {
        return this.orderdetails;
    }
    
    public void setOrderdetails(Set<OrderDetail> orderdetails) {
        this.orderdetails = orderdetails;
    }




}


