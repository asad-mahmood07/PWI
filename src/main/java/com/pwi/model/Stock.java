package com.pwi.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="stock")
public class Stock  implements java.io.Serializable {


     private int stockid;
     private Product product;
     private Warehouse warehouse;
     private Long qtybalance;

    public Stock() {
    }

	
    public Stock(int stockid) {
        this.stockid = stockid;
    }
    public Stock(int stockid, Product product, Warehouse warehouse, Long qtybalance) {
       this.stockid = stockid;
       this.product = product;
       this.warehouse = warehouse;
       this.qtybalance = qtybalance;
    }
   
     @Id 

    
    @Column(name="stockid", unique=true, nullable=false)
    public int getStockid() {
        return this.stockid;
    }
    
    public void setStockid(int stockid) {
        this.stockid = stockid;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="productid")
    public Product getProduct() {
        return this.product;
    }
    
    public void setProduct(Product product) {
        this.product = product;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="warehouseid")
    public Warehouse getWarehouse() {
        return this.warehouse;
    }
    
    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    
    @Column(name="qtybalance", precision=10, scale=0)
    public Long getQtybalance() {
        return this.qtybalance;
    }
    
    public void setQtybalance(Long qtybalance) {
        this.qtybalance = qtybalance;
    }




}


