package com.pwi.model;


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="size")
public class Size  implements java.io.Serializable {


     private int sizeid;
     private String name;
     private String description;
     private Set<Product> products = new HashSet<Product>(0);

    public Size() {
    }

	
    public Size(int sizeid) {
        this.sizeid = sizeid;
    }
    public Size(int sizeid, String name, String description, Set<Product> products) {
       this.sizeid = sizeid;
       this.name = name;
       this.description = description;
       this.products = products;
    }
   
     @Id 

    
    @Column(name="sizeid", unique=true, nullable=false)
    public int getSizeid() {
        return this.sizeid;
    }
    
    public void setSizeid(int sizeid) {
        this.sizeid = sizeid;
    }

    
    @Column(name="name")
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    
    @Column(name="description")
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="size")
    public Set<Product> getProducts() {
        return this.products;
    }
    
    public void setProducts(Set<Product> products) {
        this.products = products;
    }




}


