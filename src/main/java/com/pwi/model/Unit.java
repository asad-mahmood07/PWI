package com.pwi.model;


import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="unit")
public class Unit  implements java.io.Serializable {


     private int unitid;
     private String name;
     private String description;
     private Set<Product> products = new HashSet<Product>(0);

    public Unit() {
    }

	
    public Unit(int unitid) {
        this.unitid = unitid;
    }
    public Unit(int unitid, String name, String description, Set<Product> products) {
       this.unitid = unitid;
       this.name = name;
       this.description = description;
       this.products = products;
    }
   
     @Id 

    
    @Column(name="unitid", unique=true, nullable=false)
    public int getUnitid() {
        return this.unitid;
    }
    
    public void setUnitid(int unitid) {
        this.unitid = unitid;
    }

    
    @Column(name="name")
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    
    @Column(name="description")
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="unit")
    public Set<Product> getProducts() {
        return this.products;
    }
    
    public void setProducts(Set<Product> products) {
        this.products = products;
    }




}


